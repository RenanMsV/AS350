Eurocopter AS350 Squirrel / A - Star
-------------------------------------

http://www.fguk.eu/index.php/hangar/download/13-rotary-wing/507-eurocopter-as350-squirrel-a-star

![image](https://i.imgur.com/SF87wlm.jpg)

The Eurocopter AS350 Écureuil (Squirrel) is a single-engine light helicopter originally designed and manufactured in France by Aérospatiale (now Airbus Helicopters). In North America, the AS350 is marketed as the AStar.

RELEASE NOTES:
---------------

V2.0.2
----

* New Cockpit Glass Texture 
* New Reflection improvements
* Can now set reflection power for each livery xml
* Improvements submitted by BR-RVD


V2.0.1
----

* New Camera Views
* New Liveries
* Improvements submitted by BR-RVD 

V2.0
---

* Almost a complete rebuild of the AS350. Now based on the AS350B3+
* All new  FDM
* All new Instrument panel
* Procedural manual start up possible ( See Aircraft help in Sim ** NOTE Auto start does not currently animate everything the way is does during a manual start)
* All doors fully animated including the   floor panel  for looking down during hovering. ( Click to operate doors ).
* All new sound pack.
* Working winch operative. ( only available in configurations containing the winch. )
* New visual FX
* 14 liveries included.
* 6 configuration options including  "Everest Challenge" which is a specially lightened airframe as used during the  Everest Challenge where they attempted to land on top of Mt Everest.

V1.0.2
----

* Bug Fixes submitted by D-ECHO
* It's now completely independent of the ec130 and the bo105.
* Fix for the interior cabin texture.

V1.0.1
----

* Minor bug fix for the MP passenger to prevent others seeing the passenger as a blue glider.

V1.0
----

* This is a complete ground up rebuild of our earlier AS350.
* Fully Texture mapped.
* Livery swapping enabled. Includes a number of liveries.
* YASM FDM, using YASIM CURRENT VERSION, Suitable for FG 3.2+.
* Rembrandt ready.
* Advanced lighting kit.
* MP Sound
* MP passenger option.
* NOTE, there is currently a mapping issue on the cabin floor area.
